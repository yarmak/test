# coding: utf-8

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
import os

from flask import Flask, send_from_directory

root = os.path.dirname(os.path.abspath(__file__))
static_root = os.path.join(root, 'ami', 'build', 'static')
html_root = os.path.join(static_root, 'html')
app = Flask(__name__, static_url_path='/static', static_folder=static_root)


@app.route('/')
def index():
    return send_from_directory(html_root, 'index.html')


if __name__ == "__main__":
    app.run()