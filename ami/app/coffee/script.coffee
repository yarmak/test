app = angular.module 'ami', ['ngResource', 'ui.bootstrap', 'ngAnimate']

app.factory 'Skills',  ['$resource', ($resource) ->
  $resource '/static/json/skills.json'
]

app.factory 'Freelancers',  ['$resource', ($resource) ->
  $resource '/static/json/freelancers.json'
]

app.directive 'hmRead', ->
  restrict:'AE'
  scope:
    hmtext : '@'
    hmlimit : '@'
    hmfulltext:'@'
    hmMoreText:'@'
    hmLessText:'@'
    hmMoreClass:'@'
    hmLessClass:'@'
  templateUrl: '/static/html/show-more.html'
  controller: ($scope) ->
    $scope.toggleValue = ->
      if $scope.hmfulltext == true
        $scope.hmfulltext = false
      else if $scope.hmfulltext == false
        $scope.hmfulltext = true
      else
        $scope.hmfulltext = true



app.controller "Page1Controller", ['$scope', 'Skills', ($scope, Skills) ->
  $scope.skills = []
  $scope.fields = []
  Skills.get (result) ->

    angular.forEach result, (value, key) ->
      if not angular.isArray(value)
        return
      angular.forEach value, (item) ->
        $scope.skills.push name: item, ctg: key

      $scope.fields.push
        key: key
        label: key
        help_text: "e.g., #{value.slice(0, 5).join(', ')}"

  $scope.form = {}

  $scope.delete = (key,index) ->
    delete $scope.form[key][index]

  $scope.add = (item) ->
    $scope.form[item.ctg] = $scope.form[item.ctg] || {}
    $scope.form[item.ctg][item.name]= 'Competent'
    $scope.selected = ''
]

app.controller "Page2Controller", ['$scope', 'Freelancers', ($scope, Freelancers) ->
  $scope.freelancers = []
  $scope.losers = []
  Freelancers.query (results) ->
    $scope.freelancers = results

  $scope.reasons = [
    {name: 'Okay, but not quite right'}
    {name: 'Clearly not qualified'}
    {name: 'Qualified, but too expensive'}
    {name: 'Poor comments in work history'}
  ]
  $scope.remove = (person_index, reason_index) ->
    $scope.losers.push
      freelancer: $scope.freelancers[person_index].name
      reason: $scope.reasons[reason_index].name
    $scope.freelancers.splice(person_index, 1)
]
