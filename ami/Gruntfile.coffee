module.exports = (grunt) ->
  require('time-grunt')(grunt)

  grunt.initConfig(
    cnf:
      tmp: '.tmp'
      static: 'build/static'
      app: 'app'
      bower: 'bower'

    pkg: grunt.file.readJSON('package.json')

    less:
      build:
        files:
          '<%= cnf.tmp %>/css/less.css': ['<%= cnf.app %>/less/*.less']

    imagemin:
      images:
        files: [
          expand: true,
          cwd: '<%= cnf.app %>/images'
          src: '**/**/*.{gif,jpeg,jpg,png,ico}'
          dest: '<%= cnf.static %>/images'
        ]

    bower_concat:
      build:
        dest: '<%= cnf.tmp %>/js/bower.js'
        cssDest: '<%= cnf.tmp %>/css/bower.css'
        dependencies:
          'angular': ['jquery']
          'bootstrap': ['jquery']
        mainFiles:
          'angular': ["angular.js", "angular-csp.css"]

    coffee:
      compile:
        files:
          '<%= cnf.tmp %>/js/coffee.js': [
            '<%= cnf.app %>/coffee/**/*.coffee'
          ]


    jade:
      compile_dev:
        options:
          pretty: true
        files: [
          expand: true
          cwd: "<%= cnf.app %>/jade/"
          src: ["**/*.jade", ]
          dest: "<%= cnf.static %>/html/"
          ext: ".html"
        ]
      compile:
        files: [
          expand: true
          cwd: "<%= cnf.app %>/jade/"
          src: ["**/*.jade", ]
          dest: "<%= cnf.static %>/html/"
          ext: ".html"
        ]

    concat:
      dist:
        files:
          '<%= cnf.static %>/js/script.js': [
            '<%= cnf.tmp %>/js/*.js'
#            '<%= cnf.app %>/js/app/app.js'
          ]

      dist_css:
        files:
          '<%= cnf.static %>/css/style.css': [
            '<%= cnf.tmp %>/css/*.css'
            '<%= cnf.app %>/css/*.css'
          ]

    copy:
      fonts:
        files: [
          expand: true
          dot: true
          cwd: '<%= cnf.bower %>/bootstrap/fonts/'
          dest: '<%= cnf.static %>/fonts/'
          src: [
            '**/**/*.{eot,svg,ttf,woff,otf,woff2}'
          ]
        ]
      json:
        files: [
          expand: true
          dot: true
          cwd: '<%= cnf.app %>/json/'
          dest: '<%= cnf.static %>/json/'
          src: [
            '**/**/*.json'
          ]
        ]

    ngAnnotate:
      options:
        singleQuotes: true

      script:
        files:
          '<%= cnf.tmp %>/annotate/a.js': ['<%= cnf.static %>/js/script.js']

    uglify:
      build:
        files:
          '<%= cnf.static %>/js/script.min.js': ['<%= cnf.tmp %>/annotate/a.js']
      build2:
        files: [
          expand: true,
          cwd: '<%= cnf.static %>/js'
          src: ['*.js', '!*.min.js']
          dest: '<%= cnf.static %>/js'
          ext: '.min.js'
        ]

    cssmin:
      build:
        options:
          keepSpecialComments: 0
        files: [
          expand: true,
          cwd: '<%= cnf.static %>/css'
          src: ['*.css', '!*.min.css']
          dest: '<%= cnf.static %>/css'
          ext: '.min.css'
        ]

    watch:
      js:
        files: ['<%= cnf.app %>/js/**/*.js']
        tasks: ['concat:dist']

      coffee:
        files: ['<%= cnf.app %>/coffee/**/*.coffee']
        tasks: ['coffee', 'concat:dist']

      gruntfile:
        files: ['Gruntfile.coffee']
        tasks: ['concat']

      styles:
        files: ['<%= cnf.app %>/css/**/*.css', '<%= cnf.app %>/less/**/*.less']
        tasks: ['less', 'concat:dist_css']

      jade:
        files: ['<%= cnf.app %>/jade/**/*.jade',]
        tasks: ['jade:compile_dev',]

    clean:
      tmp:
        src: ['<%= cnf.tmp %>']
      all:
        src: ['<%= cnf.tmp %>', '<%= cnf.static %>']
  )

  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-jade')

  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-bower-concat')


  grunt.loadNpmTasks('grunt-contrib-copy')

  grunt.loadNpmTasks('grunt-contrib-cssmin')

  grunt.loadNpmTasks('grunt-ng-annotate')
  grunt.loadNpmTasks('grunt-contrib-uglify')

  grunt.loadNpmTasks('grunt-contrib-imagemin')

  grunt.loadNpmTasks('grunt-contrib-clean')

  grunt.loadNpmTasks('grunt-contrib-watch')



  grunt.registerTask('build', [
    'clean:all'
    'less'
    'coffee'
    'jade:compile'
    'imagemin'
    'bower_concat'
    'concat'
    'copy'
    'cssmin'
    'ngAnnotate'
    'uglify'

    'clean:tmp'
  ])

  grunt.registerTask('online', [
    'clean:tmp'
    'less'
    'coffee'
    'jade:compile_dev'
    'bower_concat'
    'concat'
    'watch'
  ])
